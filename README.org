#+title: Finger Server

Finger-server is a simple file server for the Finger Protocol.

* Install
Depending on the version of your Racket installation you can install in two ways:

Using raco for Racket version >= 8:

: raco pkg install https://gitlab.com/cnngimenez/finger-server.git

Racket version < 8:

#+BEGIN_SRC shell
  git clone https://gitlab.com/cnngimenez/finger-server.git
  cd finger-server
  raco pkg install
#+END_SRC

Ensure that the PATH environment variable is configured accordingly for Racket. The following path should be included in the variable: =~/.local/share/racket/$RACKET_VERSION/bin=. It is possible to call the finger-server with the full path: =racket ~/.local/share/racket/8.4/bin/finger-server=. Also, you can run the server inside the repository downloaded by using git clone.

* Running
Finger-server runs on port 79 and publish the current directory. However, it can be changed with the =-p= and =-d= parameters respectively. The following is the synopsis of the program:

: finger-server -p $PORT -d $DIRECTORY_TO_SERVE

The client to access the server is finger typically. To list the files server use the following command:

: finger @HOST

To read a particular file, use the username as the filename:

: finger FILE@HOST

To run the server and access it through localhost, see below.

In Langange or Elpher, use the following URL format to access your finger server: finger://HOST/FILE

* API Documentation
The library is documented in the scribble file at =./scribblings/*.scrbl=. When the package is installed using raco, this file can be accessed with your browser using the following command:

: raco docs finger-server

The Racket REPL can use these functions by loading the library. Only the exported functions and variables are available unless entering the module. Use the following code in racket to enter the module:

#+caption: Entering the Racket module
#+BEGIN_SRC racket
  (require finger-racket)
  ,enter finger-server
#+END_SRC

* Using finger-server in other port
The finger-server can be run at any port. However, the finger client connect directly through the TCP 79 port. It is possible to redirect the ports by using the NAT rules at the iptables firewall. The following rules will re-route all received packages at the 79 port to 1079 port. 

#+BEGIN_SRC shell
iptables -t nat -I PREROUTING -p tcp --dport 79 -j REDIRECT --to-ports 1079
iptables -t nat -I OUTPUT -p tcp -o lo --dport 79 -j REDIRECT --to-ports 1079
#+END_SRC

Two iptables rules are required to support any interface (the first rule) and the localhost interface (the second rule). The localhost rule is useful for testing and debugging the program.

* Show warnings and debugging log
Sometimes, it is required to see the server output. The logger has several levels, and the most informative is the debug level. The following command runs the server showing any log information.

: cd $FINGER-SERVER-GIT-REPOSITORY
: racket -W debug@finger-server main.rkt

* Running tests

: raco test *.rkt

* License
This work is under the GNU General Public License version 3 (GPLv3). See COPYING.txt file for more information.

Christian Gimenez.
2022.
