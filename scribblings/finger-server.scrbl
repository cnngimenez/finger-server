#lang scribble/manual
@require[@for-label[finger-server
                    racket/base
                    racket/string
                    racket/file
                    racket/tcp]]

@title{finger-server}
@author{Christian Gimenez (cnngimenez)}

@defmodule[finger-server]

Finger-server is a simple file server for the Finger Protocol.

@[table-of-contents]

@section{Identifiers used as variables}
Some of these variables are setted through parameters. For easy accessing it is used as side-effect inside the functions.

@defthing[finger-listener listener?]{
The current TCP listener.

If no listener is created, then #f is stored.
}

@defthing[finger-path parameter?]{
The path to serve.

Any files outside this path would not be served.
}

@defthing[finger-port parameter?]{
The TCP port to open to listen for connections.
}

@section{Reading the query}
The query is red from an input port. It should be an empty string, a username/filename, or a filepath followed by CR and LF characters.

Syntax validations are checked after this step. The only requirement is that the query should not be more than 1026 bytes long. If this requirement is not fulfilled, then an error is raised.

Also, the EOF should not be expected. The client must wait for the server answer after sending its query. If EOF is found, then an error is raised.

@defproc[(receive-query [in port?] [buffer bytes?])
                        (or/c exact-nonnegative-integer?
                              eof-object? procedure?)]{
Receives the query from the input port and store it into buffer. Return the number of bytes received.
If EOF is received, raise @racket['unexpected-end-of-input]. If the limit is reached (more than 1026 bytes), raise @racket['request-too-long].
}

@defproc[(read-query [in port?])
                     string?]{
Read the query from the input port. Return a string containing the query in latin-1 (it should be ASCII, but latin-1 is the more accurate approximation).
}

@section{Sintactical query validation}
A query is sintactically valid if:

@itemlist[
  @item{It does not start with "/".
  
  This avoids to reach any system file by using absolute paths.}
  @item{It does not have "..".
  
  This queries can reach files outside the path configured by the administrator.}
]

@defproc[(valid-query? [query string?]) boolean?]{
Is the query valid?

Return true if the query is considered safe and valid.
}

@section{Executing the query}
Executing, or @bold{processing}, the query consists on interpreting and doing what the query asks.

In the Finger Protocol, there are two main queries to be implemented in this case:
@itemlist[
@item{"" should send a list of files in the path provided by the administrator.}
@item{"NAME" should send the file if it exists in the path.}
]

The entry point to run the query is the @racket[process-query] function. If the developer wants to add more functionallity, they should change this function. 

@defproc[(process-query [query string?]) string?]{
Parse the query and execute the correct function.

If the query is not valid, return @racket["Query not valid."] string. Else, return the result of the query as string.
}

@subsection{Processing functions}

@defproc[(list-files) string?]{
Return all files from the path as string.
}

@defproc[(show-file-if-exists [filename string?]) string?]{
Return the file contents if exists.

If the file does not exists, return @racket["Not found."] string.
}

@section{Accepting the client}

@defproc[(accept-client [listener tcp-listener?]) void?]{
Main loop to accept a new TCP client connection and process it.

Read the query and process it accordingly. Close the client ports after processing the query.

This is an infinite loop. It stops with the Control C signal from the administrator.
}

@section{Logger}
To add a new topic, a new logger is intialized. But the parent logger, the default one, is still working. This is useful to show all debugging and warning logs from the server only. The Racket -W aprameter accepts the level and the topic to show.

@defproc[(init-logger) void?]{
Set a new logger with the finger-server topic name as the current one.
}

@section{Main}
@defproc[(finger-server-run [port exact-nonnegative-integer?]) void?]{
Start the main loop: receives client connections and process them.

Create the logger, and start accepting connections and processing queries.
Handle any break (Control C signals) or any errors by closing the connection and exiting the program.
}
